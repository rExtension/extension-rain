// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Theopse (Self@theopse.org)
// Licensed under BSD-2-Caluse
// File: extension.js (rExtension/雨落同清/extension.js)
// Content:  
// Copyright (c) 2022 Theopse Organization All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

game.import("extension", (lib, game, ui, get, ai, _status) => {
    let extInfo = {
        intro: "雨打梨花深闭门·忘了青春·误了青春",
        version: "0.0.4",
        branch: "Master",
        build: 7,
        nextPreview: 9,
        times: 1,
    };
    
    let extension = {
        name: "雨落同清",
        editable: false,
        content: (config, _pack) => {
            // Load Extension
            let today = new Date();
            lib.arenaReady.push(() => {
                // lib.extensionPack["reHeart"]
                let translate = "雨落同清";
                let extension = "Rain";
                let type = "rExtension";

                let intro2 = [
                    "<span style=\"color:#1688F2\">Author: Rintim",
                    `Version: ${
                        ["Release", "Preview"].includes(extInfo.branch) ? 
                            (extInfo.branch === "Release" ? extInfo.version : `${extInfo.version}pre`) 
                            : (extInfo.branch === "Development" ? 
                                `Build ${today.getFullYear()}.${today.getMonth() + 1}.${today.getDate()}.${extInfo.times}` 
                                : `Build ${extInfo.build}${extInfo.nextPreview === null ? "" : ` Form ${extInfo.nextPreview}`}`)
                    }`,
                    "本扩展目前为测试阶段，欢迎反馈BUG</span>",
                    "- - - - - - - - - - - - - - - - - - - - - - - - -",
                    "●仓库地址: ",
                    "- Gitlab:/Rintim/rintim-extension-rain",
                    "●前置需求: ",
                    "- reHeart",
                    "- StandardLib",
                    ""
                ]
                if (extInfo.branch === "Development") {
                    intro2.addArray([
                        "●注意: <span style=\"color:#FF3333\">你目前在使用Development分支",
                        "该分支下可能会出现BUG，请确认需求使用</span>",
                        ""
                    ])
                }
                // Set Update Strings
                {
                    lib.extensionPack[translate].version = extInfo.version;
                    let strings = [
                        "</li>",
                        "注意：",
                        "本扩展目前处于测试阶段，可能会有一些未知BUG",
                        "如发现BUG，希望能及时反馈"
                    ];
                    game.showExtensionChangeLog(strings.join("</br>"), translate);
                }
                // Set Menu
                {
                    let _delete = lib.extensionMenu[`extension_${translate}`].delete;

                    delete lib.extensionMenu[`extension_${translate}`].delete;

                    delete lib.extensionMenu[`extension_${translate}`].author;

                    lib.extensionMenu[`extension_${translate}`].des = {
                        name: `<div class="${type}_${extension}">扩展介绍<font size="5px" color="gold">⇨</font></div>`,
                        clear: true,
                        onclick: function () {
                            if (this[`${type}_${extension}_Click`] === undefined) {
                                let more = ui.create.div(`.${type}_${extension}_Click`, [
                                    "<div style=\"border: 1px solid gray\"><font size=2px>本扩展将官方武将按官方设计思路进行加强，使之能在当前环境中有发挥且符合其历史地位",
                                    "本扩展主打代码实现，按照BSD-2-Caluse协议开源，你可以在此扩展中随意拷贝代码",
                                    "如果你有好的设计，欢迎提供",
                                    "如果你打算提供代码，也非常欢迎</font></div>"
                                ].join("</br>"))
                                this.parentNode.insertBefore(more, this.nextSibling);
                                this[`${type}_${extension}_Click`] = more;
                                this.innerHTML = `<div class="${type}_${extension}">扩展介绍<font size="5px">⇩</font></div>`;
                            } else {
                                this.parentNode.removeChild(this[`${type}_${extension}_Click`]);
                                delete this[`${type}_${extension}_Click`];
                                this.innerHTML = `<div class="${type}_${extension}">扩展介绍<font size="5px">⇨</font></div>`;
                            };
                        }
                    }

                    lib.extensionMenu[`extension_${translate}`].intro2 = {
                        name: intro2.join("</br>"),
                        clear: true,
                        nopointer: true,
                    }

                    lib.extensionMenu[`extension_${translate}`]["config_title"] = {
                        "name": "<p align=center><span style=\"font-size:18px\">- - - - - - - 武将设置 - - - - - - -</span>",
                        "clear": true,
                        "nopointer": true
                    };

                    lib.extensionMenu[`extension_${translate}`]["character_enable"] = {
                        name: "武将启用",
                        intro: "开启此功能后重启生效。启用扩展包中的武将。",
                        init: true,
                    };

                    lib.extensionMenu[`extension_${translate}`]["dynamic_enable"] = {
                        name: "使用动态皮肤",
                        intro: "开启此功能后重启生效。使用武将的动态皮肤，部分武将无动态皮肤（请自行注意开销）。",
                        init: false,
                    };

                    lib.extensionMenu[`extension_${translate}`]["colour_enable"] = {
                        name: "使用彩色描述",
                        intro: "（WIP）开启此功能后重启生效。使用彩色描述，高亮重点标识",
                        init: false,
                    };

                    lib.extensionMenu[`extension_${translate}`]["namic_enable"] = {
                        name: "启用武将前缀",
                        intro: "开启此功能后重启生效。启用扩展包中武将的前缀「雨」。",
                        init: false,
                    };

                    lib.extensionMenu[`extension_${translate}`]["Last BR"] = {
                        "name": "</br>",
                        "clear": true,
                        "nopointer": true
                    };

                    lib.extensionMenu[`extension_${translate}`].delete = _delete;
                }
                // Check Dependence
                {
                    let bool = true;
                    let abort = [];
                    if (!lib.storage["reHeart"]) {
                        bool = false;
                        abort.push("reHeart");
                    }
                    if (!lib.storage["rExtension_StandardLib"]) {
                        bool = false;
                        abort.push("StandardLib");
                    }

                    if (!bool) {
                        alert("雨落同清: 扩展读取错误！\n缺少前置: " + abort);
                        /* lib.extensionPack[name].intro */ lib.extensionMenu[`extension_${translate}`].intro.name = `<span style=\"color:#FF3333\">缺少前置: ${abort.join(",")}</br>扩展停止读取</span></br></br>${intro}`;
                        return;
                    }
                }
                // Add Skill
                if (config.character_enable) {
                    let skills = {
                        "rExtension_Rain_Skill_liubei_rende": {
                            name: "仁德",
                            des: "出牌阶段，你可以将任意张牌交给一名其他角色；若你于此阶段内已给出两张以上的牌，你回复一点体力或视为使用一张基本牌。",
                            desColour: "出牌阶段，你可以将任意张牌交给一名其他角色；若你于此阶段内已给出两张以上的牌，你回复一点体力或视为使用一张基本牌。",
                            content: {
                                audio: ["rende", "ext:雨落同清/audio/skill:2"],
                                enable: "phaseUse",
                                filterCard: true,
                                selectCard: [1, Infinity],
                                discard: false,
                                position: "he",
                                lose: false,
                                init: (player, skill) => {
                                    if (!player.storage[skill]) player.storage[skill] = 0;
                                },
                                filter: (event, player) => {
                                    return player.hasCards("he");
                                },
                                delay: false,
                                filterTarget: (card, player, target) => {
                                    return player != target;
                                },
                                check: (card) => {
                                    if (ui.selected.cards.length && ui.selected.cards[0].name == "du") return 0;
                                    if (!ui.selected.cards.length && card.name == "du") return 20;
                                    var player = get.owner(card);
                                    if (ui.selected.cards.length >= Math.max(2, player.countCards("h") - player.hp)) return 0;
                                    if (player.hp == player.maxHp || player.storage["rExtension_Rain_Skill_liubei_rende"] < 0 || player.countCards("h") <= 1) {
                                        var players = game.filterPlayer();
                                        for (var i = 0; i < players.length; i++) {
                                            if (players[i].hasSkill("haoshi") &&
                                                !players[i].isTurnedOver() &&
                                                !players[i].hasJudge("lebu") &&
                                                get.attitude(player, players[i]) >= 3 &&
                                                get.attitude(players[i], player) >= 3) {
                                                return 11 - get.value(card);
                                            }
                                        }
                                        if (player.countCards("h") > player.hp) return 10 - get.value(card);
                                        if (player.countCards("h") > 2) return 6 - get.value(card);
                                        return -1;
                                    }
                                    return 10 - get.value(card);
                                },
                                content: () => {
                                    "step 0"
                                    var evt = _status.event.getParent("phaseUse");
                                    if (evt && evt.name == "phaseUse" && !evt.rExtrende) {
                                        var next = game.createEvent("rExtrende_clear");
                                        _status.event.next.remove(next);
                                        evt.after.push(next);
                                        evt.rExtrende = true;
                                        next.player = player;
                                        next.setContent(() => {
                                            player.storage["rExtension_Rain_Skill_liubei_rende"] = 0;
                                        });
                                    }
                                    target.gain(cards, player, "giveAuto");
                                    if (typeof player.storage[event.name] != "number") {
                                        player.storage[event.name] = 0;
                                    }
                                    if (player.storage[event.name] >= 0) {
                                        player.storage[event.name] += cards.length;
                                        if (player.storage[event.name] >= 2) {
                                            var list = [];
                                            if (lib.filter.cardUsable({ name: "sha" }, player, event.getParent("chooseToUse")) && game.hasPlayer(function (current) {
                                                return player.canUse("sha", current);
                                            })) {
                                                list.push(["基本", "", "sha"]);
                                            }
                                            for (var i of lib.inpile_nature) {
                                                if (lib.filter.cardUsable({ name: "sha", nature: i }, player, event.getParent("chooseToUse")) && game.hasPlayer(function (current) {
                                                    return player.canUse({ name: "sha", nature: i }, current);
                                                })) {
                                                    list.push(["基本", "", "sha", i]);
                                                }
                                            }
                                            if (lib.filter.cardUsable({ name: "tao" }, player, event.getParent("chooseToUse")) && game.hasPlayer(function (current) {
                                                return player.canUse("tao", current);
                                            })) {
                                                list.push(["基本", "", "tao"]);
                                            }
                                            if (lib.filter.cardUsable({ name: "jiu" }, player, event.getParent("chooseToUse")) && game.hasPlayer(function (current) {
                                                return player.canUse("jiu", current);
                                            })) {
                                                list.push(["基本", "", "jiu"]);
                                            }
                                            if (list.length) {
                                                player.chooseButton(["是否视为使用一张基本牌？", [list, "vcard"]]).set("ai", function (button) {
                                                    var player = _status.event.player;
                                                    var card = { name: button.link[2], nature: button.link[3] };
                                                    if (card.name == "tao") {
                                                        if (player.hp == 1 || (player.hp == 2 && !player.hasShan()) || player.needsToDiscard()) {
                                                            return 5;
                                                        }
                                                        return 1;
                                                    }
                                                    if (card.name == "sha") {
                                                        if (game.hasPlayer(function (current) {
                                                            return player.canUse(card, current) && get.effect(current, card, player, player) > 0
                                                        })) {
                                                            if (card.nature == "fire") return 2.95;
                                                            if (card.nature == "thunder" || card.nature == "ice") return 2.92;
                                                            return 2.9;
                                                        }
                                                        return 0;
                                                    }
                                                    if (card.name == "jiu") {
                                                        return 0.5;
                                                    }
                                                    return 0;
                                                });
                                            }
                                            else {
                                                event.finish();
                                            }
                                            player.storage[event.name] = -1;
                                        }
                                        else {
                                            event.finish();
                                        }
                                    }
                                    else {
                                        event.finish();
                                    }
                                    "step 1"
                                    if (result && result.bool && result.links[0]) {
                                        var card = { name: result.links[0][2], nature: result.links[0][3] };
                                        player.chooseUseTarget(card, true);
                                    }
                                    else {
                                        if (player.hp < player.maxHp) player.recover();
                                    }
                                },
                                ai: {
                                    fireAttack: true,
                                    order: function (skill, player) {
                                        if (player.hp < player.maxHp && player.storage["rExtension_Rain_Skill_liubei_rende"] < 2 && player.countCards("h") > 1) {
                                            return 10;
                                        }
                                        return 4;
                                    },
                                    result: {
                                        target: function (player, target) {
                                            if (target.hasSkillTag("nogain")) return 0;
                                            if (ui.selected.cards.length && ui.selected.cards[0].name == "du") {
                                                if (target.hasSkillTag("nodu")) return 0;
                                                return -10;
                                            }
                                            if (target.hasJudge("lebu")) return 0;
                                            var nh = target.countCards("h");
                                            var np = player.countCards("h");
                                            if (player.hp == player.maxHp || player.storage["rExtension_Rain_Skill_liubei_rende"] < 0 || player.countCards("h") <= 1) {
                                                if (nh >= np - 1 && np <= player.hp && !target.hasSkill("haoshi")) return 0;
                                            }
                                            return Math.max(1, 5 - nh);
                                        }
                                    },
                                    effect: {
                                        target: (card, player, target) => {
                                            if (player == target && get.type(card) == "equip") {
                                                if (player.countCards("e", { subtype: get.subtype(card) })) {
                                                    if (game.hasPlayer(function (current) {
                                                        return current != player && get.attitude(player, current) > 0;
                                                    })) {
                                                        return 0;
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    threaten: 0.8
                                }
                            }
                        },
                        "rExtension_Rain_Skill_liubei_jijiang_meta": {
                            name: "激将",
                            des: "当你需要使用或打出【杀】时，你可以令其他角色依次选择是否打出一张【杀】，若有角色响应，则你视为使用或打出了此【杀】，且若你的身份为主公，其摸一张牌；当一名其他角色使用或打出【杀】时，其可以令你摸一张牌，若你的身份为主公，其摸一张牌。",
                            desColour: "当你需要使用或打出【杀】时，你可以令其他角色依次选择是否打出一张【杀】，若有角色响应，则你视为使用或打出了此【杀】，且若你的身份为主公，其摸一张牌；当一名其他角色使用或打出【杀】时，其可以令你摸一张牌，若你的身份为主公，其摸一张牌。",
                            content: {
                                audio: ["jijiang", "ext:雨落同清/audio/skill:2"],
                                group: ["rExtension_Rain_Skill_liubei_jijiang", "rExtension_Rain_Skill_liubei_jijiang_ask", "rExtension_Rain_Skill_liubei_jijiang_use"],
                            }
                        },
                        "rExtension_Rain_Skill_liubei_jijiang": {
                            name: "激将",
                            des: "当你需要使用或打出【杀】时，你可以令其他角色依次选择是否打出一张【杀】，若有角色响应，则你视为使用或打出了此【杀】，且若你的身份为主公，其摸一张牌。",
                            desColour: "当你需要使用或打出【杀】时，你可以令其他角色依次选择是否打出一张【杀】，若有角色响应，则你视为使用或打出了此【杀】，且若你的身份为主公，其摸一张牌。",
                            content: {
                                audio: ["jijiang", "ext:雨落同清/audio/skill:2"],
                                filter: (event, player) => {
                                    if (!game.hasPlayer(function (current) {
                                        return current != player;
                                    })) return false;
                                    return !event.jijiang && (event.type != "phase" || !player.hasSkill("jijiang3"));
                                },
                                enable: ["chooseToUse", "chooseToRespond"],
                                viewAs: { name: "sha" },
                                filterCard: function () { return false },
                                selectCard: -1,
                                ai: {
                                    order: function () {
                                        return get.order({ name: "sha" }) + 0.3;
                                    },
                                    respondSha: true,
                                    skillTagFilter: function (player) {
                                        if (!game.hasPlayer(function (current) {
                                            return current != player;
                                        })) return false;
                                    },
                                },
                            }
                        },
                        "rExtension_Rain_Skill_liubei_jijiang_ask": {
                            name: "激将",
                            content: {
                                audio: ["jijiang", "ext:雨落同清/audio/skill:2"],
                                trigger: { player: ["useCardBegin", "respondBegin"] },
                                logTarget: "targets",
                                filter: function (event, player) {
                                    return event.skill == "rExtension_Rain_Skill_liubei_jijiang";
                                },
                                forced: true,
                                content: function () {
                                    "step 0"
                                    delete trigger.skill;
                                    trigger.getParent().set("jijiang", true);
                                    "step 1"
                                    if (event.current == undefined) event.current = player.next;
                                    if (event.current == player) {
                                        player.addTempSkill("jijiang3");
                                        event.finish();
                                        trigger.cancel();
                                        trigger.getParent().goto(0);
                                    }
                                    else {
                                        var next = event.current.chooseToRespond("是否替" + get.translation(player) + "打出一张杀？", { name: "sha" });
                                        next.set("ai", function () {
                                            var event = _status.event;
                                            return (get.attitude(event.player, event.source) - 1);
                                        });
                                        next.set("source", player);
                                        next.set("jijiang", true);
                                        next.set("skillwarn", "替" + get.translation(player) + "打出一张杀");
                                        next.noOrdering = true;
                                        next.autochoose = lib.filter.autoRespondSha;
                                    }
                                    "step 2"
                                    if (result.bool) {
                                        event.finish();
                                        trigger.card = result.card;
                                        trigger.cards = result.cards;
                                        trigger.throw = false;
                                        if (typeof event.current.ai.shown == "number" && event.current.ai.shown < 0.95) {
                                            event.current.ai.shown += 0.3;
                                            if (event.current.ai.shown > 0.95) event.current.ai.shown = 0.95;
                                        }
                                        if (player.isZhu) event.current.draw("nodelay");
                                    }
                                    else {
                                        event.current = event.current.next;
                                        event.goto(1);
                                    }
                                }
                            }
                        },
                        "rExtension_Rain_Skill_liubei_jijiang_use": {
                            name: "激将",
                            des: "当一名其他角色使用或打出【杀】时，其可以令你摸一张牌，若你的身份为主公，其摸一张牌。",
                            desColour: "当一名其他角色使用或打出【杀】时，其可以令你摸一张牌，若你的身份为主公，其摸一张牌。",
                            content: {
                                trigger: { global: ["useCard", "respond"] },
                                direct: true,
                                filter: function (event, player) {
                                    return event.card.name == "sha" && event.player != player && event.player.isIn();
                                },
                                content: function () {
                                    "step 0"
                                    trigger.player.chooseBool("激将：是否令" + get.translation(player) + "摸一张牌？").set("ai", function () {
                                        var evt = _status.event;
                                        return get.attitude(evt.player, evt.getParent().player) > 0;
                                    });
                                    "step 1"
                                    if (result.bool) {
                                        player.logSkill("rExtension_Rain_Skill_liubei_jijiang");
                                        trigger.player.line(player, "fire");

                                        if (player.isZhu) game.asyncDraw([player, trigger.player]);
                                        else player.draw("nodelay");
                                    }
                                },
                            }
                        },
                        "rExtension_Rain_Skill_guanyu_wusheng": {
                            name: "武圣",
                            des: "你可以将一张牌当「杀」使用或打出；你使用红色杀无距离限制且使用黑色杀造成的伤害+1。",
                            desColour: "你可以将一张牌当「杀」使用或打出；你使用红色杀无距离限制且使用黑色杀造成的伤害+1。",
                            content: {
                                audio: ["wusheng", "ext:雨落同清/audio/skill:2"],
                                enable: ["chooseToRespond", "chooseToUse"],
                                filterCard: function (card, player) {
                                    return true;
                                },
                                position: "hes",
                                group: "rExtension_Rain_Skill_guanyu_damageAdd",
                                viewAs: { name: "sha" },
                                viewAsFilter: function (player) {
                                    if (!player.countCards("hes")) return false;
                                },
                                prompt: "将一张牌当杀使用或打出",
                                check: function (card) { return 4 - get.value(card) },
                                ai: {
                                    skillTagFilter: function (player) {
                                        if (!player.countCards("hes")) return false;
                                    },
                                    respondSha: true,
                                },
                                mod: {
                                    targetInRange: function (card) {
                                        if (get.color(card) == "red" && card.name == "sha") return true;
                                    },
                                }
                            }
                        },
                        "rExtension_Rain_Skill_guanyu_damageAdd": {
                            content: {
                                audio: "rExtension_Rain_Skill_guanyu_wusheng",
                                trigger: { player: "useCard1" },
                                forced: true,
                                popup: false,
                                silent: true,
                                firstDo: true,
                                filter: function (event, player) {
                                    return event.card && event.card.name == "sha" && get.color(event.card) == "black";
                                },
                                content: function () {
                                    trigger.baseDamage++;
                                }
                            }
                        },
                        "rExtension_Rain_Skill_guanyu_yijue": {
                            name: "义绝",
                            des: "出牌阶段限一次，你可以展示一张手牌并令一名有手牌的其他角色展示一张手牌并比较两张牌的点数：若你展示牌的点数大于其，其弃置此牌，然后本回合其非锁定技失效、不能使用或打出牌且你对其使用牌无距离及次数限制；反之你获得此牌，并可以令其回复一点体力。",
                            desColour: "出牌阶段限一次，你可以展示一张手牌并令一名有手牌的其他角色展示一张手牌并比较两张牌的点数：若你展示牌的点数大于其，其弃置此牌，然后本回合其非锁定技失效、不能使用或打出牌且你对其使用牌无距离及次数限制；反之你获得此牌，并可以令其回复一点体力。",
                            content: {
                                audio: ["yijue", "ext:雨落同清/audio/skill:2"],
                                enable: "phaseUse",
                                usable: 1,
                                position: "h",
                                filterTarget: function (card, player, target) {
                                    return player != target && target.countCards("h");
                                },
                                filterCard: true,
                                check: function (card) {
                                    return get.number(card);
                                },
                                discard: false,
                                lose: false,
                                content: function () {
                                    "step 0"
                                    player.showCards(cards);
                                    target.chooseCard(true).ai = function (card) {
                                        var player = _status.event.player;
                                        if ((player.hasShan() || player.hp < 3) && get.color(card) == "black") return 0.5;
                                        return Math.max(1, 20 - get.value(card));
                                    };
                                    "step 1"
                                    target.showCards(result.cards);
                                    event.card2 = result.cards[0];
                                    /*
                                    if (get.color(event.card2) == "black") {
                                        if (!target.hasSkill("fengyin")) {
                                            target.addTempSkill("fengyin");
                                        }
                                        target.addTempSkill("new_yijue2");
                                        event.finish();
                                    }
                                    else {
                                        player.gain(event.card2, target, "give", "bySelf");
                                        if (target.hp < target.maxHp) {
                                            player.chooseBool("是否让目标回复一点体力？").ai = function (event, player) {
                                                return get.recoverEffect(target, player, player) > 0;
                                            };
                                        }
                                    }
                                    */
                                    let num = get.number(cards[0]);
                                    let num2 = get.number(result.cards[0]);
                                    if (num > num2) {
                                        target.discard(result.cards);
                                        if (!target.hasSkill("fengyin")) {
                                            target.addTempSkill("fengyin");
                                        }
                                        target.addTempSkill("rExtension_Rain_Skill_guanyu_yijue2");
                                        event.finish();
                                    }
                                    else {
                                        player.gain(event.card2, target, "give", "bySelf");
                                        if (target.hp < target.maxHp) {
                                            player.chooseBool("是否让目标回复一点体力？").ai = function (event, player) {
                                                return get.recoverEffect(target, player, player) > 0;
                                            };
                                        }
                                    }
                                    "step 2"
                                    if (result.bool) {
                                        target.recover();
                                    }
                                },
                                mod: {
                                    cardUsableTarget: function (card, player, target) {
                                        if (target.hasSkill("rExtension_Rain_Skill_guanyu_yijue2")) return true;
                                    },
                                    targetInRange: function (card, player, target) {
                                        if (target.hasSkill("rExtension_Rain_Skill_guanyu_yijue2")) return true;
                                    },
                                }
                            }
                        },
                        "rExtension_Rain_Skill_guanyu_yijue2": {
                            name: "义绝",
                            content: {
                                mark: true,
                                mod: {
                                    cardEnabled: function () {
                                        return false;
                                    },
                                    cardRespondable: function () {
                                        return false;
                                    },
                                    cardSavable: function () {
                                        return false;
                                    }
                                },
                                intro: {
                                    content: "不能使用或打出卡牌"
                                }
                            }
                        },
                        "rExtension_Rain_Skill_zhaoyun_longdan": {
                            name: "龙胆",
                            des: "你可以将「杀」当「闪」，「闪」当「杀」，「酒」当「桃」，「桃」当「酒」使用或打出；若如此做，你摸一张牌。",
                            desColour: "你可以将「杀」当「闪」，「闪」当「杀」，「酒」当「桃」，「桃」当「酒」使用或打出；若如此做，你摸一张牌。",
                            content: {
                                mod: {
                                    aiValue: function (player, card, num) {
                                        if (card.name != "sha" && card.name != "shan") return;
                                        var geti = function () {
                                            var cards = player.getCards("hs", function (card) {
                                                return card.name == "sha" || card.name == "shan";
                                            });
                                            if (cards.contains(card)) {
                                                return cards.indexOf(card);
                                            }
                                            return cards.length;
                                        };
                                        return Math.max(num, [7, 5, 5, 3][Math.min(geti(), 3)]);
                                    },
                                    aiUseful: function () {
                                        return lib.skill["rExtension_Rain_Skill_zhaoyun_longdan"].mod.aiValue.apply(this, arguments);
                                    },
                                },
                                group: "rExtension_Rain_Skill_zhaoyun_longdan_draw",
                                locked: false,
                                audio: ["longdan", "ext:雨落同清/audio/skill:2"],
                                hiddenCard: function (player, name) {
                                    if (name == "tao") return player.countCards("hs", "jiu") > 0;
                                    if (name == "jiu") return player.countCards("hs", "tao") > 0;
                                    return false;
                                },
                                enable: ["chooseToUse", "chooseToRespond"],
                                position: "hs",
                                prompt: "将杀当做闪，或将闪当做杀，或将桃当做酒，或将酒当做桃使用或打出",
                                viewAs: function (cards, player) {
                                    var name = false;
                                    switch (get.name(cards[0], player)) {
                                        case "sha": name = "shan"; break;
                                        case "shan": name = "sha"; break;
                                        case "tao": name = "jiu"; break;
                                        case "jiu": name = "tao"; break;
                                    }
                                    if (name) return { name: name };
                                    return null;
                                },
                                check: function (card) {
                                    var player = _status.event.player;
                                    if (_status.event.type == "phase") {
                                        var max = 0;
                                        var name2;
                                        var list = ["sha", "tao", "jiu"];
                                        var map = { sha: "shan", tao: "jiu", jiu: "tao" }
                                        for (var i = 0; i < list.length; i++) {
                                            var name = list[i];
                                            if (player.countCards("hs", map[name]) > (name == "jiu" ? 1 : 0) && player.getUseValue({ name: name }) > 0) {
                                                var temp = get.order({ name: name });
                                                if (temp > max) {
                                                    max = temp;
                                                    name2 = map[name];
                                                }
                                            }
                                        }
                                        if (name2 == get.name(card, player)) return 1;
                                        return 0;
                                    }
                                    return 1;
                                },
                                filterCard: function (card, player, event) {
                                    event = event || _status.event;
                                    var filter = event._backup.filterCard;
                                    var name = get.name(card, player);
                                    if (name == "sha" && filter({ name: "shan", cards: [card] }, player, event)) return true;
                                    if (name == "shan" && filter({ name: "sha", cards: [card] }, player, event)) return true;
                                    if (name == "tao" && filter({ name: "jiu", cards: [card] }, player, event)) return true;
                                    if (name == "jiu" && filter({ name: "tao", cards: [card] }, player, event)) return true;
                                    return false;
                                },
                                filter: function (event, player) {
                                    var filter = event.filterCard;
                                    if (filter({ name: "sha" }, player, event) && player.countCards("hs", "shan")) return true;
                                    if (filter({ name: "shan" }, player, event) && player.countCards("hs", "sha")) return true;
                                    if (filter({ name: "tao" }, player, event) && player.countCards("hs", "jiu")) return true;
                                    if (filter({ name: "jiu" }, player, event) && player.countCards("hs", "tao")) return true;
                                    return false;
                                },
                                ai: {
                                    respondSha: true,
                                    respondShan: true,
                                    skillTagFilter: function (player, tag) {
                                        var name;
                                        switch (tag) {
                                            case "respondSha": name = "shan"; break;
                                            case "respondShan": name = "sha"; break;
                                        }
                                        if (!player.countCards("hs", name)) return false;
                                    },
                                    order: function (item, player) {
                                        if (player && _status.event.type == "phase") {
                                            var max = 0;
                                            var list = ["sha", "tao", "jiu"];
                                            var map = { sha: "shan", tao: "jiu", jiu: "tao" }
                                            for (var i = 0; i < list.length; i++) {
                                                var name = list[i];
                                                if (player.countCards("hs", map[name]) > (name == "jiu" ? 1 : 0) && player.getUseValue({ name: name }) > 0) {
                                                    var temp = get.order({ name: name });
                                                    if (temp > max) max = temp;
                                                }
                                            }
                                            if (max > 0) max += 0.3;
                                            return max;
                                        }
                                        return 4;
                                    },
                                },
                            }
                        },
                        "rExtension_Rain_Skill_zhaoyun_longdan_draw": {
                            content: {
                                trigger: { player: ["useCard", "respond"] },
                                silent: true,
                                filter: function (event, player) {
                                    return event.skill == "rExtension_Rain_Skill_zhaoyun_longdan";
                                },
                                content: function () {
                                    player.draw();
                                }
                            }
                        },
                        "rExtension_Rain_Skill_zhaoyun_chongzhen": {
                            name: "冲阵",
                            des: "群势力技，当你对一名其他角色使用基本牌，或用基本牌响应一名其他角色使用的牌时，你可以弃置其一张牌；若你发动了「龙胆」，你将「弃置」改为「获得」",
                            desColour: "<b>群势力技，</b>当你对一名其他角色使用基本牌，或用基本牌响应一名其他角色使用的牌时，你可以弃置其一张牌；若你发动了「龙胆」，你将「弃置」改为「获得」",
                            content: {
                                group: ["rExtension_Rain_Skill_zhaoyun_chongzhen1", "rExtension_Rain_Skill_zhaoyun_chongzhen2", "rExtension_Rain_Skill_zhaoyun_chongzhen_check"],
                                audio: ["chongzhen", "ext:雨落同清/audio/skill:2"],
                                ai: {
                                    combo: "rExtension_Rain_Skill_zhaoyun_longdan",
                                    mingzhi: false,
                                    effect: {
                                        target: function (card, player, target, current) {
                                            if (get.tag(card, "respondShan") || get.tag(card, "respondSha")) {
                                                if (get.attitude(target, player) <= 0) {
                                                    if (current > 0) return;
                                                    if (target.countCards("h") == 0) return 1.6;
                                                    if (target.countCards("h") == 1) return 1.2;
                                                    if (target.countCards("h") == 2) return [0.8, 0.2, 0, -0.2];
                                                    return [0.4, 0.7, 0, -0.7];
                                                }
                                            }
                                        },
                                    },
                                }
                            }
                        },
                        "rExtension_Rain_Skill_zhaoyun_chongzhen_check": {
                            name: "冲阵",
                            content: {
                                trigger: {
                                    global: "gameStart",
                                    player: "enterGame",
                                },
                                charlotte: true,
                                silent: true,
                                content: () => {
                                    player.checkGroupSkill("rExtension_Rain_Skill_zhaoyun_chongzhen", "qun");
                                }
                            }
                        },
                        "rExtension_Rain_Skill_zhaoyun_chongzhen1": {
                            name: "冲阵",
                            content: {
                                audio: ["chongzhen", "ext:雨落同清/audio/skill:2"],
                                trigger: { player: "useCard" },
                                filter: function (event, player) {
                                    //if ((event.card.name != "sha" && event.card.name != "shan") || (event.skill != "longdan_shan" && event.skill != "longdan_sha" &&
                                    //    event.skill != "fanghun_shan" && event.skill != "fanghun_sha" && event.skill != "ollongdan")) return false;
                                    if (get.type(event.card, "trick") !== "basic") return false;
                                    if (event.respondTo && event.respondTo.length) return event.respondTo[0] && event.respondTo[0] !== player && ["rExtension_Rain_Skill_zhaoyun_longdan"].includes(event.skill) ? event.respondTo[0].countGainableCards(player, "he") > 0 : event.respondTo[0].countDiscardableCards(player, "he") > 0;
                                    if (event.targets.every(current => current === player)) return false;
                                    return event.targets.some(target => {
                                        return target && ["rExtension_Rain_Skill_zhaoyun_longdan"].includes(event.skill) ? target.countGainableCards(player, "he") > 0 : target.countDiscardableCards(player, "he") > 0;
                                    });
                                },
                                prompt2: "当你对一名其他角色使用基本牌，你可以弃置其一张牌；若你发动了「龙胆」，你将「弃置」改为「获得」",
                                content: function () {
                                    //var target = lib.skill["rExtension_Rain_Skill_zhaoyun_chongzhen1"].logTarget(trigger, player);
                                    if (trigger.respondTo) {
                                        ["rExtension_Rain_Skill_zhaoyun_longdan"].includes(trigger.skill) ? player.gainPlayerCard(trigger.respondTo[0], "he", true) : player.discardPlayerCard(trigger.respondTo[0], "he", true);
                                        return;
                                    }
                                    let targets = trigger.targets;
                                    targets.filter(current => current !== player && ["rExtension_Rain_Skill_zhaoyun_longdan"].includes(event.skill) ? current.countGainableCards(player, "he") > 0 : current.countDiscardableCards(player, "he") > 0).forEach(target => ["rExtension_Rain_Skill_zhaoyun_longdan"].includes(trigger.skill) ? player.gainPlayerCard(target, "he", true) : player.discardPlayerCard(target, "he", true));
                                }
                            }
                        },
                        "rExtension_Rain_Skill_zhaoyun_chongzhen2": {
                            name: "冲阵",
                            content: {
                                audio: ["chongzhen", "ext:雨落同清/audio/skill:2"],
                                trigger: { player: "respond" },
                                filter: function (event, player) {
                                    if (event.source === player) return false;
                                    return event.source && ["rExtension_Rain_Skill_zhaoyun_longdan"].includes(event.skill) ? event.source.countGainableCards(player, "he") > 0 : event.source.countDiscardableCards(player, "he") > 0;
                                },
                                logTarget: "source",
                                prompt2: "当你用基本牌响应一名其他角色使用的牌时，你可以弃置其一张牌；若你发动了「龙胆」，你将「弃置」改为「获得」",
                                content: function () {
                                    let target = trigger.source;
                                    ["rExtension_Rain_Skill_zhaoyun_longdan"].includes(trigger.skill) ? player.gainPlayerCard(target, "he", true) : player.discardPlayerCard(target, "he", true)
                                }
                            }
                        },
                        "rExtension_Rain_Skill_zhaoyun_yajiao": {
                            name: "涯角",
                            des: "蜀势力技，当你使用或打出一张牌时，你可以展示牌堆顶一张牌，然后可将这张牌置于弃牌堆；若此牌与你使用/打出的牌类型一致，你可令一名角色获得此牌，反之若当前回合角色不是你，你可弃置一名角色区域内一张牌。",
                            desColour: "<b>蜀势力技，</b>当你使用或打出一张牌时，你可以展示牌堆顶一张牌，然后可将这张牌置于弃牌堆；若此牌与你使用/打出的牌类型一致，你可令一名角色获得此牌，反之若当前回合角色不是你，你可弃置一名角色区域内一张牌。",
                            content: {
                                audio: ["yajiao", "ext:雨落同清/audio/skill:2"],
                                trigger: { player: ["respond", "useCard"] },
                                frequent: true,
                                filter: function (event, player) {
                                    return get.itemtype(event.cards) == "cards";
                                },
                                group: "rExtension_Rain_Skill_zhaoyun_yajiao_check",
                                content: function () {
                                    "step 0"
                                    event.card = get.cards()[0];
                                    game.broadcast(function (card) {
                                        ui.arena.classList.add("thrownhighlight");
                                        card.copy("thrown", "center", "thrownhighlight", ui.arena).animate("start");
                                    }, event.card);
                                    event.node = event.card.copy("thrown", "center", "thrownhighlight", ui.arena).animate("start");
                                    ui.arena.classList.add("thrownhighlight");
                                    game.addVideo("thrownhighlight1");
                                    game.addVideo("centernode", null, get.cardInfo(event.card));
                                    if (get.type(event.card, "trick") == get.type(trigger.card, "trick")) {
                                        player.chooseTarget("选择获得此牌的角色").set("ai", function (target) {
                                            var att = get.attitude(_status.event.player, target);
                                            if (_status.event.du) {
                                                if (target.hasSkillTag("nodu")) return 0;
                                                return -att;
                                            }
                                            if (att > 0) {
                                                return att + Math.max(0, 5 - target.countCards("h"));
                                            }
                                            return att;
                                        }).set("du", event.card.name == "du");
                                    }
                                    else {
                                        player.chooseBool("是否弃置" + get.translation(event.card) + "？");
                                        event.disbool = true;
                                    }
                                    game.delay(2);
                                    "step 1"
                                    if (event.disbool) {
                                        if (!result.bool) {
                                            game.log(player, "展示了", event.card);
                                            ui.cardPile.insertBefore(event.card, ui.cardPile.firstChild);
                                        }
                                        else {
                                            game.log(player, "展示并弃掉了", event.card);
                                            event.card.discard();
                                        }
                                        game.addVideo("deletenode", player, [get.cardInfo(event.node)]);
                                        event.node.delete();
                                        game.broadcast(function (card) {
                                            ui.arena.classList.remove("thrownhighlight");
                                            if (card.clone) {
                                                card.clone.delete();
                                            }
                                        }, event.card);
                                    }
                                    else if (result.targets) {
                                        player.line(result.targets, "green");
                                        result.targets[0].gain(event.card, "log");
                                        event.node.moveDelete(result.targets[0]);
                                        game.addVideo("gain2", result.targets[0], [get.cardInfo(event.node)]);
                                        game.broadcast(function (card, target) {
                                            ui.arena.classList.remove("thrownhighlight");
                                            if (card.clone) {
                                                card.clone.moveDelete(target);
                                            }
                                        }, event.card, result.targets[0]);
                                    }
                                    else {
                                        game.log(player, "展示并弃掉了", event.card);
                                        event.card.discard();
                                        game.addVideo("deletenode", player, [get.cardInfo(event.node)]);
                                        event.node.delete();
                                        game.broadcast(function (card) {
                                            ui.arena.classList.remove("thrownhighlight");
                                            if (card.clone) {
                                                card.clone.delete();
                                            }
                                        }, event.card);
                                    }
                                    game.addVideo("thrownhighlight2");
                                    ui.arena.classList.remove("thrownhighlight");
                                    "step 2"
                                    if (event.disbool && player !== _status.currentPhase) {
                                        player.chooseTarget("是否弃置一名角色区域内的一张牌？", function (card, player, target) {
                                            return target.countDiscardableCards(player, "hej") > 0;
                                        }).set("ai", function (target) {
                                            var player = _status.event.player;
                                            return get.effect(target, { name: "guohe" }, player, player);
                                        });
                                    }
                                    else event.finish();
                                    "step 3"
                                    if (result.bool) {
                                        player.line(result.targets[0], "green");
                                        player.discardPlayerCard(result.targets[0], "hej", true);
                                    }
                                },
                                ai: {
                                    effect: {
                                        target: function (card, player, target) {
                                            if (get.tag(card, "respond") && target.countCards("h") > 1) return [1, 0.2];
                                        }
                                    }
                                }
                            }
                        },
                        "rExtension_Rain_Skill_zhaoyun_yajiao_check": {
                            name: "涯角",
                            content: {
                                trigger: {
                                    global: "gameStart",
                                    player: "enterGame",
                                },
                                charlotte: true,
                                silent: true,
                                content: () => {
                                    player.checkGroupSkill("rExtension_Rain_Skill_zhaoyun_yajiao", "shu");
                                }
                            }
                        },
                        "rExtension_Rain_Skill_simayi_fankui": {
                            name: "反馈",
                            des: "当你受到一点伤害时，你可以获得伤害来源的一张牌并摸一张牌。",
                            desColour: "当你受到一点伤害时，你可以获得伤害来源的一张牌并摸一张牌。",
                            content: {
                                audio: ["fankui", "ext:雨落同清/audio/skill:2"],
                                trigger: { player: "damageEnd" },
                                direct: true,
                                filter: function (event, player) {
                                    return (event.source && event.source.countGainableCards(player, "he") && event.num > 0 && event.source != player);
                                },
                                content: function () {
                                    "step 0"
                                    event.count = trigger.num;
                                    "step 1"
                                    --event.count;
                                    player.gainPlayerCard(get.prompt(event.name, trigger.source), trigger.source, get.buttonValue, "he").set("logSkill", [event.name, trigger.source]);
                                    "step 2"
                                    if (result.bool) {
                                        player.draw(1, "nodelay");
                                        if (event.count > 0 && trigger.source.countGainableCards(player, "he") > 0) event.goto(1);
                                    }
                                },
                                ai: {
                                    maixie_defend: true,
                                    effect: {
                                        target: function (card, player, target) {
                                            if (player.countCards("he") > 1 && get.tag(card, "damage")) {
                                                if (player.hasSkillTag("jueqing", false, target)) return [1, -1.5];
                                                if (get.attitude(target, player) < 0) return [1, 1];
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        "rExtension_Rain_Skill_simayi_guicai": {
                            name: "鬼才",
                            des: "当一名角色的判定牌即将生效时，你可以打出一张牌替代之；若判定趋势因此改变，你摸一张牌。",
                            desColour: "当一名角色的判定牌即将生效时，你可以打出一张牌替代之；若判定趋势因此改变，你摸一张牌。",
                            content: {
                                audio: ["guicai", "ext:雨落同清/audio/skill:2"],
                                trigger: { global: "judge" },
                                direct: true,
                                filter: function (event, player) {
                                    return player.countCards("hes") > 0;
                                },
                                content: function () {
                                    "step 0"
                                    player.chooseCard(get.translation(trigger.player) + "的" + (trigger.judgestr || "") + "判定为" +
                                        get.translation(trigger.player.judging[0]) + "，" + get.prompt(event.name), "hes", function (card) {
                                            var player = _status.event.player;
                                            var mod2 = game.checkMod(card, player, "unchanged", "cardEnabled2", player);
                                            if (mod2 != "unchanged") return mod2;
                                            var mod = game.checkMod(card, player, "unchanged", "cardRespondable", player);
                                            if (mod != "unchanged") return mod;
                                            return true;
                                        }).set("ai", function (card) {
                                            var trigger = _status.event.getTrigger();
                                            var player = _status.event.player;
                                            var judging = _status.event.judging;
                                            var result = trigger.judge(card) - trigger.judge(judging);
                                            var attitude = get.attitude(player, trigger.player);
                                            if (attitude == 0 || result == 0) return 0;
                                            if (attitude > 0) {
                                                return result - get.value(card) / 2;
                                            }
                                            else {
                                                return -result - get.value(card) / 2;
                                            }
                                        }).set("judging", trigger.player.judging[0]);
                                    "step 1"
                                    if (result.bool) {
                                        player.respond(result.cards, "rExtension_Rain_Skill_simayi_guicai", "highlight", "noOrdering");
                                    }
                                    else {
                                        event.finish();
                                    }
                                    "step 2"
                                    if (result.bool) {
                                        player.gain(trigger.player.judging[0], "gain2");
                                        if (trigger.judge(result.cards[0]) != trigger.judge(trigger.player.judging[0])) player.draw(1, "nodelay");
                                        if (trigger.player.judging[0].clone) {
                                            trigger.player.judging[0].clone.classList.remove("thrownhighlight");
                                            game.broadcast(function (card) {
                                                if (card.clone) {
                                                    card.clone.classList.remove("thrownhighlight");
                                                }
                                            }, trigger.player.judging[0]);
                                            game.addVideo("deletenode", player, get.cardsInfo([trigger.player.judging[0].clone]));
                                        }
                                        //game.cardsDiscard(trigger.player.judging[0]);
                                        trigger.player.judging[0] = result.cards[0];
                                        trigger.orderingCards.addArray(result.cards);
                                        game.log(trigger.player, "的判定牌改为", result.cards[0]);
                                    }
                                    "step 3"
                                    game.delay(2);
                                },
                                ai: {
                                    rejudge: true,
                                    tag: {
                                        rejudge: 1,
                                    }
                                }
                            }
                        },
                        "rExtension_Rain_Skill_guojia_tiandu": {
                            name: "天妒",
                            des: "当你的判定牌生效后，你可以获得之。若此时判定趋势偏向失利或无利，你摸一张牌。",
                            desColour: "当你的判定牌生效后，你可以获得之。若此时判定趋势偏向失利或无利，你摸一张牌。",
                            content: {
                                audio: ["tiandu", "ext:雨落同清/audio/skill:2"],
                                trigger: {
                                    player: "judgeEnd",
                                },
                                filter: (event, player) => {
                                    var bool1 = get.position(event.result.card, true) == "o";
                                    var bool2 = event.result.bool;
                                    return bool1 || !bool2;
                                },
                                frequent: (event) => {
                                    if (event.result.card.name == "du") return false;
                                    if (get.mode() == "guozhan") return false;
                                    return true;
                                },
                                check: (event) => {
                                    if (event.result.card.name == "du") return false;
                                    return true;
                                },
                                direct: true,
                                content: () => {
                                    "step 0"
                                    var bool1 = get.position(trigger.result.card, true) == "o";
                                    var bool2 = trigger.result.bool;
                                    player.chooseBool(`###是否发动【${get.translation(event.name)}】（当前趋势：${bool2 == false ? "失利" : (bool2 == true ? "有利" : "无利")}）？###${bool1 ? (bool2 ? "当你的判定牌生效时，你可以获得之。" : get.translation(`${event.name}_info`)) : "当你的判定牌生效时，若此时判定趋势偏向失利或无利，你摸一张牌"}`).set("frequentSkill", event.name);
                                    "step 1"
                                    if (!result.bool) {
                                        event.finish();
                                        return;
                                    }
                                    player.logSkill(event.name);
                                    if (get.position(trigger.result.card, true) == "o") {
                                        player.gain(trigger.result.card, "gain2")
                                    }
                                    if (!trigger.result.bool) {
                                        player.draw(1);
                                    }
                                }
                            }
                        },
                        "rExtension_Rain_Skill_guojia_yiji": {
                            name: "遗计",
                            des: "当你受到一点伤害或失去一点体力后，你可以摸三张牌并弃置区域里的一张牌，然后你可以将至多两张牌分配给其他角色；当你以此法将一张牌交给其他角色时，你可将此牌至于其武将牌上，令其于出牌阶段开始时获得。",
                            desColour: "当你受到一点伤害或失去一点体力后，你可以摸三张牌并弃置区域里的一张牌，然后你可以将至多两张牌分配给其他角色；当你以此法将一张牌交给其他角色时，你可将此牌至于其武将牌上，令其于出牌阶段开始时获得。",
                            content: {
                                audio: ["yiji", "ext:雨落同清/audio/skill:2"],
                                trigger: {
                                    player: ["damageEnd", "loseHpEnd"],
                                },
                                frequent: true,
                                filter: (event) => {
                                    return event.num > 0;
                                },
                                content: () => {
                                    "step 0"
                                    event.count = 1;
                                    "step 1"
                                    player.draw(3, "nodelay");
                                    let bool = player.countCards("j") > 0;
                                    player[bool ? "discardPlayerCard" : "chooseToDiscard"](bool ? player : "请弃置一张牌", true, 1, "hej");
                                    event.given = 0;
                                    event.temp = Array.new();
                                    "step 2"
                                    player.chooseCardTarget({
                                        filterCard: true,
                                        selectCard: [1, 2],
                                        filterTarget: (card, player, target) => {
                                            return player != target && !event.temp.contains(target);
                                        },
                                        ai1: (card) => {
                                            if (ui.selected.cards.length > 0) return -1;
                                            if (card.name == "du") return 20;
                                            return (_status.event.player.countCards("h") - _status.event.player.hp);
                                        },
                                        ai2: (target) => {
                                            let att = get.attitude(_status.event.player, target);
                                            if (ui.selected.cards.length && ui.selected.cards[0].name == "du") {
                                                if (target.hasSkillTag("nodu")) return 0;
                                                return 1 - att;
                                            }
                                            return att - 4;
                                        },
                                        prompt: "请选择要送人的卡牌",
                                        position: "he",
                                    });
                                    "step 3"
                                    if (result.bool) {
                                        player.line(result.targets, "green");
                                        result.targets[0].gain(result.cards, player, "giveAuto");
                                        event.given += result.cards.length;
                                        event.cards = result.cards;
                                        event.target = result.targets[0];
                                        player.chooseCardButton("选择任意张牌置于其武将牌上", false, event.cards, [1, event.cards.length]).set("ai", button => {
                                            if (ui.selected.buttons.length == 0) return 1;
                                            return 0;
                                        });
                                    }
                                    else if (event.count < trigger.num) {
                                        event.temp = Array.new();
                                        ++event.count;
                                        event.goto(5);
                                    }
                                    else event.finish();
                                    "step 4"
                                    if (result.bool) {
                                        var cards = result.links.dup();
                                        event.target.addToExpansion(cards, "give", event.target, "bySelf", false).gaintag.add("rExtension_Rain_Skill_guojia_yiji_storage");
                                        if (!event.target.hasSkill("rExtension_Rain_Skill_guojia_yiji_storage")) event.target.addSkill("rExtension_Rain_Skill_guojia_yiji_storage");
                                        if (!event.target.storage["rExtension_Rain_Skill_guojia_yiji_storage"]) event.target.storage["rExtension_Rain_Skill_guojia_yiji_storage"] = player;
                                        // event.target.lose(cards, ui.special, "toStorage");
                                        // if (!event.target.storage["rExtension_Rain_Skill_guojia_yiji_storage"]) {
                                        //    event.target.addSkill("rExtension_Rain_Skill_guojia_yiji_storage");
                                        //    event.target.storage["rExtension_Rain_Skill_guojia_yiji_storage"] = Array.new();
                                        // }
                                        // event.target.storage["rExtension_Rain_Skill_guojia_yiji_storage"] = event.target.storage["rExtension_Rain_Skill_guojia_yiji_storage"].concat(cards);
                                        // game.addVideo("toStorage", event.target, ["rExtension_Rain_Skill_guojia_yiji_storage", get.cardsInfo(event.target.storage["rExtension_Rain_Skill_guojia_yiji_storage"]), "cards"]);
                                    }
                                    if (event.given < 2) {
                                        event.temp.push(event.target);
                                        delete event.target;
                                        delete event.cards;
                                        event.goto(2);
                                    }
                                    else if (event.count < trigger.num) {
                                        event.temp = Array.new();
                                        delete event.target;
                                        delete event.cards;
                                        ++event.count;
                                    }
                                    else event.finish();
                                    "step 5"
                                    player.chooseBool(get.prompt2(event.name)).set("frequentSkill", event.name);
                                    "step 6"
                                    if (result.bool) {
                                        player.logSkill(event.name);
                                        event.goto(1);
                                    }
                                },
                                ai: {
                                    maixie: true,
                                    maixie_hp: true,
                                    result: {
                                        effect: (card, player, target) => {
                                            if (get.tag(card, "damage")) {
                                                // if(player.hasSkillTag("jueqing", false, target)) return [1,-2];
                                                if (!target.hasFriend()) return;
                                                let num = 1;
                                                if (get.attitude(player, target) > 0) {
                                                    if (player.needsToDiscard()) {
                                                        num = 0.7;
                                                    }
                                                    else {
                                                        num = 0.5;
                                                    }
                                                }
                                                if (player.hp >= 4) return [1, num * 2];
                                                if (target.hp == 3) return [1, num * 1.5];
                                                if (target.hp == 2) return [1, num * 0.5];
                                            }
                                        }
                                    },
                                    threaten: 0.6
                                }
                            }
                        },
                        "rExtension_Rain_Skill_guojia_yiji_storage": {
                            name: "遗计",
                            content: {
                                trigger: {
                                    player: "phaseUseBegin"
                                },
                                //mark: true,
                                silent: true,
                                audio: false,
                                charlotte: true,
                                content: () => {
                                    "step 0"
                                    player.popup("遗计拿牌");
                                    player.$draw(player.countExpansions("rExtension_Rain_Skill_guojia_yiji_storage"));
                                    player.gain(player.getExpansions("rExtension_Rain_Skill_guojia_yiji_storage"));
                                    game.log(player, "获得了", player.storage["rExtension_Rain_Skill_guojia_yiji_storage"], "的", get.skillTranslation("rExtension_Rain_Skill_guojia_yiji", player), "牌");

                                    "step 1"
                                    delete player.storage["rExtension_Rain_Skill_guojia_yiji_storage"];
                                    /*
                                    while (player.countExpansions("rExtension_Rain_Skill_guojia_yiji_storage")) {
                                        let cards = player.getExpansions("rExtension_Rain_Skill_guojia_yiji_storage");
                                        cards.forEach(card => {
                                            if (get.owner(card) === player || get.position(card) === "o") return;
                                            player.loseToSpecial(card);
                                        })
                                    }
                                    */
                                    player.removeSkill("rExtension_Rain_Skill_guojia_yiji_storage");
                                    game.delay();
                                },
                                onremove: (player, skill) => {
                                    let cards = player.getExpansions(skill);
                                    if (cards.length) player.loseToDiscardpile(cards);
                                },
                                intro: {
                                    markcount: (_storage, player) => player.countExpansions("rExtension_Rain_Skill_guojia_yiji_storage"),
                                    content: (_storage, player, skill) => `共有${get.cnNumber(player.countExpansions(skill))}张牌`
                                }
                            }
                        },
                        "rExtension_Rain_Skill_liuchen_zhanjue": {
                            name: "战绝",
                            des: "出牌阶段，你可以将所有手牌当作【决斗】使用；若如此做，当你以此法造成伤害时，你令伤害值+X（X为本回合你以此法造成伤害的次数）此【决斗】结算后，你与以此法受到伤害的角色各摸一张牌；若你在同一阶段内以此法摸了三张或更多的牌，此技能失效直到回合结束。",
                            desColour: "出牌阶段，你可以将所有手牌当作【决斗】使用；若如此做，当你以此法造成伤害时，你令伤害值+X（X为本回合你以此法造成伤害的次数）此【决斗】结算后，你与以此法受到伤害的角色各摸一张牌；若你在同一阶段内以此法摸了三张或更多的牌，此技能失效直到回合结束。",
                            content: {
                                audio: ["zhanjue", "ext:雨落同清/audio/skill:2"],
                                init: (player, skill) => {
                                    player.storage[skill] = 0;
                                },
                                enable: "phaseUse",
                                filterCard: true,
                                selectCard: -1,
                                position: "h",
                                filter: function (event, player) {
                                    if (player.getStat().skill.zhanjue_draw && player.getStat().skill.zhanjue_draw >= 3) return false;
                                    var hs = player.getCards("h");
                                    if (!hs.length) return false;
                                    for (var i = 0; i < hs.length; i++) {
                                        var mod2 = game.checkMod(hs[i], player, "unchanged", "cardEnabled2", player);
                                        if (mod2 === false) return false;
                                    }
                                    return true;
                                },
                                viewAs: { name: "juedou" },
                                group: ["rExtension_Rain_Skill_liuchen_zhanjue2", "rExtension_Rain_Skill_liuchen_zhanjue_damage"],
                                ai: {
                                    damage: true,
                                    order: 1,
                                    effect: {
                                        player: function (card, player, target) {
                                            if (_status.event.skill == "zhanjue") {
                                                if (player.hasSkillTag("directHit_ai", true, {
                                                    target: target,
                                                    card: card,
                                                }, true)) return;
                                                if (player.countCards("h") >= 3 || target.countCards("h") >= 3) return "zeroplayertarget";
                                                if (player.countCards("h", "tao")) return "zeroplayertarget";
                                                if (target.countCards("h", "sha") > 1) return "zeroplayertarget";
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        "rExtension_Rain_Skill_liuchen_zhanjue2": {
                            content: {
                                audio: false,
                                trigger: { player: "useCardAfter" },
                                forced: true,
                                popup: false,
                                filter: function (event, player) {
                                    return event.skill == "rExtension_Rain_Skill_liuchen_zhanjue";
                                },
                                content: function () {
                                    "step 0"
                                    var stat = player.getStat().skill;
                                    if (!stat.zhanjue_draw) stat.zhanjue_draw = 0;
                                    stat.zhanjue_draw++;
                                    player.draw("nodelay");
                                    var list = game.filterPlayer(function (current) {
                                        if (current.getHistory("damage", function (evt) {
                                            return evt.card == trigger.card;
                                        }).length > 0) {
                                            if (current == player) {
                                                stat.zhanjue_draw++;
                                            }
                                            return true;
                                        }
                                        return false;
                                    });
                                    if (list.length) {
                                        list.sortBySeat();
                                        game.asyncDraw(list);
                                    }
                                    "step 1"
                                    game.delay();
                                }
                            }
                        },
                        "rExtension_Rain_Skill_liuchen_zhanjue_damage": {
                            name: "战绝",
                            content: {
                                audio: ["zhanjue_damage", "ext:雨落同清/audio/skill:2"],
                                trigger: { source: "damageBegin" },
                                locked: true,
                                direct: true,
                                filter: (event, player) => {
                                    return event.getParent().skill == "rExtension_Rain_Skill_liuchen_zhanjue";
                                },
                                content: () => {
                                    var evt = _status.event.getParent("phaseUse");
                                    if (evt && evt.name == "phaseUse" && !evt.rExtzhanjue) {
                                        var next = game.createEvent("rExtzhanjue_clear");
                                        _status.event.next.remove(next);
                                        evt.after.push(next);
                                        evt.rExtzhanjue = true;
                                        next.player = player;
                                        next.setContent(() => {
                                            player.storage["rExtension_Rain_Skill_liuchen_zhanjue"] = 0;
                                        });
                                    }
                                    if (typeof player.storage["rExtension_Rain_Skill_liuchen_zhanjue"] != "number") {
                                        player.storage["rExtension_Rain_Skill_liuchen_zhanjue"] = 0;
                                    }
                                    if (player.storage["rExtension_Rain_Skill_liuchen_zhanjue"]) {
                                        player.logSkill(event.name);
                                        trigger.num += player.storage["rExtension_Rain_Skill_liuchen_zhanjue"];
                                    }
                                    ++player.storage["rExtension_Rain_Skill_liuchen_zhanjue"];
                                },
                            }
                        },
                        "rExtension_Rain_Skill_liuchen_qinwang_meta": {
                            name: "勤王",
                            des: "当你需要使用或打出一张【杀】时，你可以弃置一张牌，然后视为你发动〖激将①〗；若有角色响应，则该角色打出【杀】时摸一张牌；若你的身份为主公，你发动〖激将①〗无需弃牌且你可以此法发动〖护驾】",
                            desColour: "当你需要使用或打出一张【杀】时，你可以弃置一张牌，然后视为你发动〖激将①〗；若有角色响应，则该角色打出【杀】时摸一张牌；若你的身份为主公，你发动〖激将①〗无需弃牌且你可以此法发动〖护驾】",
                            content: {
                                audio: ["qinwang", "ext:雨落同清/audio/skill:2"],
                                group: ["rExtension_Rain_Skill_liuchen_qinwang", "rExtension_Rain_Skill_liuchen_qinwang_ask", "rExtension_Rain_Skill_liuchen_qinwang_shan"],
                            }
                        },
                        "rExtension_Rain_Skill_liuchen_qinwang": {
                            name: "勤王",
                            des: "当你需要使用或打出一张【杀】时，你可以弃置一张牌，然后视为你发动〖激将①〗；若有角色响应，则该角色打出【杀】时摸一张牌；若你的身份为主公，你发动〖激将①〗无需弃牌。",
                            desColour: "当你需要使用或打出一张【杀】时，你可以弃置一张牌，然后视为你发动〖激将①〗；若有角色响应，则该角色打出【杀】时摸一张牌；若你的身份为主公，你发动〖激将①〗无需弃牌。",
                            content: {
                                audio: ["qinwang", "ext:雨落同清/audio/skill:2"],
                                filter: function (event, player) {
                                    if (!game.hasPlayer(function (current) {
                                        return current != player;
                                    }) || (!player.isZhu && !player.countCards("he"))) return false;
                                    return !event.jijiang && (event.type != "phase" || !player.hasSkill("jijiang3"));
                                },
                                enable: ["chooseToUse", "chooseToRespond"],
                                viewAs: {
                                    name: "sha",
                                    cards: [],
                                    suit: "none",
                                    number: null,
                                    isCard: true,
                                },
                                selectCard: () => {
                                    return _status.event.player.isZhu ? -1 : 1;
                                },
                                filterCard: (card, player, event) => {
                                    if (player.isZhu) return false;
                                    return lib.filter.cardDiscardable(card, player, event);
                                },
                                position: "he",
                                check: function (card) {
                                    var player = _status.event.player, players = game.filterPlayer();
                                    if (player.hasSkill("qinwang_ai")) return false;
                                    for (var i = 0; i < players.length; i++) {
                                        var nh = players[i].countCards("h");
                                        if (players[i] != player && players[i].group == "shu" && get.attitude(players[i], player) > 2 && (nh >= 3 && players[i].countCards("h", "sha"))) {
                                            return 5 - get.value(card);
                                        }
                                    }
                                    return 0;
                                },
                                ai: {
                                    order: function () {
                                        return get.order({ name: "sha" }) - 0.3;
                                    },
                                    respondSha: true,
                                    skillTagFilter: function (player) {
                                        if (!game.hasPlayer(function (current) {
                                            return current != player;
                                        }) || (!player.isZhu && !player.countCards("he"))) return false;
                                    },
                                },
                            }
                        },
                        "rExtension_Rain_Skill_liuchen_qinwang_ask": {
                            name: "勤王",
                            content: {
                                audio: ["qinwang", "ext:雨落同清/audio/skill:2"],
                                trigger: { player: ["useCardBegin", "respondBegin"] },
                                logTarget: "targets",
                                filter: function (event, player) {
                                    return ["rExtension_Rain_Skill_liuchen_qinwang", "rExtension_Rain_Skill_liuchen_qinwang_shan"].contains(event.skill);
                                },
                                forced: true,
                                content: function () {
                                    "step 0"
                                    event.askill = trigger.skill;
                                    delete trigger.skill;
                                    delete trigger.card.cards;
                                    player.discard(trigger.cards);
                                    delete trigger.cards;
                                    trigger.getParent().set("jijiang", true);
                                    "step 1"
                                    if (event.current == undefined) event.current = player.next;
                                    if (event.current == player) {
                                        player.addTempSkill("jijiang3");
                                        player.addTempSkill("qinwang_ai");
                                        event.finish();
                                        trigger.cancel();
                                        trigger.getParent().goto(0);
                                    }
                                    else {
                                        var next =
                                            (event.askill == "rExtension_Rain_Skill_liuchen_qinwang_shan") ?
                                                event.current.chooseToRespond("是否替" + get.translation(player) + "打出一张闪？", { name: "shan" })
                                                : event.current.chooseToRespond("是否替" + get.translation(player) + "打出一张杀？", { name: "sha" });
                                        next.set("ai", function () {
                                            var event = _status.event;
                                            return (get.attitude(event.player, event.source) - 2);
                                        });
                                        next.set("source", player);
                                        next.set("jijiang", true);
                                        next.set("skillwarn", "替" + get.translation(player) + "打出一张" + (event.askill == "rExtension_Rain_Skill_liuchen_qinwang_shan") ? "闪" : "杀");
                                        next.noOrdering = true;
                                        next.autochoose = lib.filter.autoRespondSha;
                                    }
                                    "step 2"
                                    if (result.bool) {
                                        delete trigger.skill;
                                        event.current.draw("nodelay");
                                        event.finish();
                                        trigger.card = result.card;
                                        trigger.cards = result.cards;
                                        trigger.throw = false;
                                        if (typeof event.current.ai.shown == "number" && event.current.ai.shown < 0.95) {
                                            event.current.ai.shown += 0.3;
                                            if (event.current.ai.shown > 0.95) event.current.ai.shown = 0.95;
                                        }
                                    }
                                    else {
                                        event.current = event.current.next;
                                        event.goto(1);
                                    }
                                }
                            }
                        },
                        "rExtension_Rain_Skill_liuchen_qinwang_shan": {
                            name: "勤王",
                            des: "当你需要使用或打出一张【闪】时，你可以令其他角色依次选择是否打出一张【闪】，若有角色响应，则你视为使用或打出了此【闪】且该角色摸一张牌。",
                            desColour: "当你需要使用或打出一张【闪】时，你可以令其他角色依次选择是否打出一张【闪】，若有角色响应，则你视为使用或打出了此【闪】且该角色摸一张牌。",
                            content: {
                                audio: ["qinwang", "ext:雨落同清/audio/skill:2"],
                                filter: function (event, player) {
                                    if (!player.isZhu) return false;
                                    if (!event.filterCard({ name: "shan", isCard: true }, player, event)) return false;
                                    if (event.name != "chooseToUse" && !lib.filter.cardRespondable({ name: "shan", isCard: true }, player, event)) return false;
                                    if (!game.hasPlayer(function (current) {
                                        return current != player;
                                    })) return false;
                                    return !event.jijiang && (event.type != "phase" || !player.hasSkill("jijiang3"));
                                },
                                enable: ["chooseToUse", "chooseToRespond"],
                                viewAs: {
                                    name: "shan",
                                    cards: [],
                                    suit: "none",
                                    number: null,
                                    isCard: true,
                                },
                                filterCard: function () { return false },
                                selectCard: -1,
                                position: "he",
                                ai: {
                                    order: function () {
                                        return get.order({ name: "shan" }) - 0.3;
                                    },
                                    respondShan: true,
                                    skillTagFilter: function (player) {
                                        if (!player.isZhu) return false;
                                        if (!game.hasPlayer(function (current) {
                                            return current != player;
                                        })) return false;
                                    },
                                }
                            }
                        },
                        "rExtension_Rain_Skill_huangyueying_jizhi": {
                            name: "集智",
                            des: "当你使用非转化锦囊牌时，你可以摸一张牌；若你以此法摸的牌不为锦囊牌，你再摸一张牌；当你对一名其他角色使用锦囊牌时，你可以明示其一张牌。",
                            desColour: "当你使用非转化锦囊牌时，你可以摸一张牌；若你以此法摸的牌不为锦囊牌，你再摸一张牌；当你对一名其他角色使用锦囊牌时，你可以明示其一张牌。",
                            content: {
                                audio: lib.storage["rExtension_charaLib"] ? ["mutil", "rExtension_Rain_Skill_huangyueying_jizhi1", "rExtension_Rain_Skill_huangyueying_jizhi2"] : ["jizhi1", "ext:雨落同清/audio/skill:2"],
                                frequent: true,
                                group: ["rExtension_Rain_Skill_huangyueying_jizhi1", "rExtension_Rain_Skill_huangyueying_jizhi2"]
                            }
                        },
                        "rExtension_Rain_Skill_huangyueying_jizhi1": {
                            name: "集智",
                            des: "当你使用非转化锦囊牌时，你可以摸一张牌；若你以此法摸的牌不为锦囊牌，你再摸一张牌。",
                            desColour: "当你使用非转化锦囊牌时，你可以摸一张牌；若你以此法摸的牌不为锦囊牌，你再摸一张牌。",
                            content: {
                                audio: ["jizhi1", "ext:雨落同清/audio/skill:2"],
                                trigger: {
                                    player: "useCard"
                                },
                                frequent: () => {
                                    return !lib.config.autoskilllist.contains("rExtension_Rain_Skill_huangyueying_jizhi");
                                },
                                preHidden: true,
                                filter: function (event) {
                                    return (["trick", "delay"].includes(get.type(event.card)) && event.card.isCard);
                                },
                                content: function () {
                                    "step 0"
                                    player.draw();
                                    "step 1"
                                    if (result && result.length) {
                                        if (get.type(result[0], "trick") !== "trick") {
                                            player.draw();
                                        }
                                    }
                                },
                                ai: {
                                    threaten: 1.4,
                                    noautowuxie: true,
                                }
                            }
                        },
                        "rExtension_Rain_Skill_huangyueying_jizhi2": {
                            name: "集智",
                            des: "当你对一名其他角色使用锦囊牌时，你可以明示其一张牌。",
                            desColour: "当你对一名其他角色使用锦囊牌时，你可以明示其一张牌。",
                            content: {
                                audio: ["jizhi2", "ext:雨落同清/audio/skill:2"],
                                trigger: {
                                    player: "useCardToTargeted"
                                },
                                filter: (event, player) => {
                                    return get.type(event.card, "trick") === "trick" && event.target && event.target !== player && event.target.hasCards("h", card => !card.hasGaintag("visualCard"));
                                },
                                direct: true,
                                content: function () {
                                    "step 0"
                                    if (trigger.target.countCards("h", card => !card.hasGaintag("visualCard")) <= 1) player.chooseBool(get.prompt2(event.name, trigger.target));
                                    else player.choosePlayerCard(trigger.target, "h", 1, get.prompt2(event.name, trigger.target)).set("onlyUnVisualed", true);
                                    "step 1"
                                    if (result.bool) {
                                        player.logSkill(event.name, trigger.target);
                                        let card;
                                        if (result.links && result.links.length) {
                                            card = result.links[0];
                                        }
                                        else card = trigger.target.getCards("h", card => !card.gaintag.includes("visualCard"))[0];
                                        trigger.target.addGaintag(card, "visualCard");
                                        trigger.target.showCards([card]);
                                    }
                                },
                            }
                        },
                        "rExtension_Rain_Skill_huangyueying_qicai": {
                            name: "奇才",
                            des: "锁定技，你使用锦囊牌无距离限制；你的装备牌无法被弃置与获得。",
                            desColour: "<b>锁定技，</b>你使用锦囊牌无距离限制；你的装备牌无法被弃置与获得。",
                            content: {
                                audio: false,
                                mod: {
                                    targetInRange: (card, _player, _target, _now) => {
                                        var type = get.type(card);
                                        if (type === "trick" || type === "delay") return true;
                                    },
                                    canBeDiscarded: (card) => {
                                        if (get.position(card) === "e") return false;
                                    },
                                    canBeGained: (card) => {
                                        if (get.position(card) === "e") return false;
                                    }
                                },
                            }
                        }
                    }

                    for (let [skill, info] of skills) {
                        let { name, des, desColour, content } = info;
                        if (content) {
                            lib.skill[skill] = content;
                            if (name) lib.translate[skill] = name;
                            if (des || desColour) lib.translate[`${skill}_info`] = config.colour_enable ? (desColour ? desColour : des) : des;
                        }
                    }

                    if (!lib.skill["_doublegroup_choice"]) lib.skill["_doublegroup_choice"] = {
                        trigger: {
                            global: "gameStart",
                            player: "enterGame",
                        },
                        forced: true,
                        charlotte: true,
                        firstDo: true,
                        popup: false,
                        filter: function (event, player) {
                            return get.mode() != "guozhan" && get.is.double(player.name1) && !player._groupChosen;
                        },
                        content: function () {
                            "step 0"
                            player._groupChosen = true;
                            player.chooseControl(get.is.double(player.name1, true)).set("prompt", "请选择你的势力");
                            "step 1"
                            player.changeGroup(result.control);
                        },
                    }
                }
                // Add Character
                if (config.character_enable) {
                    let dot = config.dynamic_enable ? ".gif" : ".png";
                    let namic = config.namic_enable ? "雨" : "";
                    let characters = {
                        "rExtension_Rain_Character_Standard_liubei": {
                            sex: "male",
                            group: "shu",
                            hp: 4,
                            replace: "liubei",
                            maxHp: 4,
                            rank: { rarity: "epic" },
                            translate: namic + "刘备",
                            skills: ["rExtension_Rain_Skill_liubei_rende", "rExtension_Rain_Skill_liubei_jijiang_meta"],
                            tags: [
                                "zhu",
                                "ext:雨落同清/image/character/liubei" + dot,
                                "died:ext:雨落同清/audio/die/liubei.mp3",
                            ],
                            des: "先主姓刘，讳备，字玄德，涿郡涿县人，汉景帝子中山靖王胜之后也。以仁德治天下。",
                            catelogy: "Standard"
                        },
                        "rExtension_Rain_Character_Standard_guanyu": {
                            sex: "male",
                            group: "shu",
                            hp: 4,
                            replace: "guanyu",
                            maxHp: 4,
                            rank: { rarity: "epic" },
                            translate: namic + "关羽",
                            skills: ["rExtension_Rain_Skill_guanyu_wusheng", "rExtension_Rain_Skill_guanyu_yijue"],
                            tags: [
                                "ext:雨落同清/image/character/guanyu" + dot,
                                "died:ext:雨落同清/audio/die/guanyu.mp3",
                            ],
                            des: "字云长，本字长生，并州河东解州人。五虎上将之首，爵至汉寿亭侯，谥曰“壮缪侯”。被奉为“关圣帝君”，崇为“武圣”。",
                            catelogy: "Standard"
                        },
                        "rExtension_Rain_Character_Standard_simayi": {
                            sex: "male",
                            group: "wei",
                            hp: 3,
                            replace: "simayi",
                            maxHp: 4,
                            rank: { rarity: "epic" },
                            translate: namic + "司马懿",
                            skills: ["rExtension_Rain_Skill_simayi_fankui", "rExtension_Rain_Skill_simayi_guicai"],
                            tags: [
                                "ext:雨落同清/image/character/simayi" + dot,
                                "died:ext:雨落同清/audio/die/simayi.mp3",
                            ],
                            des: "晋宣帝，字仲达，河内温人。曾任职过曹魏的大都督，太尉，太傅。少有奇节，聪明多大略，博学洽闻，伏膺儒教，世之鬼才也。",
                            catelogy: "Standard"
                        },
                        "rExtension_Rain_Character_Standard_guojia": {
                            sex: "male",
                            group: "wei",
                            hp: 3,
                            replace: "guojia",
                            maxHp: 3,
                            rank: { rarity: "epic" },
                            translate: namic + "郭嘉",
                            skills: ["rExtension_Rain_Skill_guojia_tiandu", "rExtension_Rain_Skill_guojia_yiji"],
                            tags: [
                                "ext:雨落同清/image/character/guojia" + dot,
                                "died:ext:雨落同清/audio/die/guojia.mp3",
                            ],
                            des: "字奉孝，颍川阳翟人，官至军师祭酒。惜天妒英才，英年早逝。有诗云：“良计环环不遗策，每临制变满座惊”",
                            catelogy: "Standard"
                        },
                        "db_rExtension_Rain_Character_Standard_zhaoyun": {
                            sex: "male",
                            group: "shu",
                            hp: 4,
                            replace: "zhaoyun",
                            maxHp: 4,
                            rank: { rarity: "legend" },
                            translate: namic + "赵云",
                            skills: ["rExtension_Rain_Skill_zhaoyun_longdan", "rExtension_Rain_Skill_zhaoyun_chongzhen", "rExtension_Rain_Skill_zhaoyun_yajiao"],
                            tags: [
                                "doublegroup:shu:qun",
                                "ext:雨落同清/image/character/zhaoyun" + dot,
                                "died:ext:雨落同清/audio/die/zhaoyun.mp3",
                            ],
                            des: "字子龙，常山真定人。身长八尺，姿颜雄伟。长坂坡单骑救阿斗，先主云：“子龙一身都是胆也。”",
                            catelogy: "Standard"
                        },
                        "rExtension_Rain_Character_Standard_huangyueying": {
                            sex: "female",
                            group: "shu",
                            hp: 3,
                            replace: "huangyueying",
                            maxHp: 3,
                            rank: { rarity: "epic" },
                            translate: namic + "黄月英",
                            skills: ["rExtension_Rain_Skill_huangyueying_jizhi", "rExtension_Rain_Skill_huangyueying_qicai"],
                            tags: [
                                "doublegroup:shu:qun",
                                "ext:雨落同清/image/character/huangyueying" + dot,
                                "died:ext:雨落同清/audio/die/huangyueying.mp3",
                            ],
                            des: "荆州沔南白水人，沔阳名士黄承彦之女，诸葛亮之妻，诸葛瞻之母。容貌甚丑，而有奇才：上通天文，下察地理，韬略近于诸书无所不晓，诸葛亮在南阳闻其贤而迎娶。",
                            catelogy: "Standard"
                        },
                        "rExtension_Rain_Character_Spirit_liuchen": {
                            sex: "male",
                            group: "shu",
                            hp: 4,
                            replace: "liuchen",
                            maxHp: 4,
                            rank: { rarity: "legend" },
                            translate: namic + "刘谌",
                            skills: ["rExtension_Rain_Skill_liuchen_zhanjue", "rExtension_Rain_Skill_liuchen_qinwang_meta"],
                            tags: [
                                "zhu",
                                "ext:雨落同清/image/character/liuchen" + dot,
                                "died:ext:雨落同清/audio/die/liuchen.mp3",
                            ],
                            des: "刘禅第五子，自幼聪明，英敏过人。魏军兵临城下时，刘禅准备投降，刘谌劝阻刘禅投降不成后悲愤不已，遂自杀于昭烈庙。",
                            catelogy: "Spirit"
                        },
                    }

                    lib.characterPack[`mode_extension_${type}_${extension}`] = {};

                    for (let [name, info] of characters) {
                        let showHp;
                        if (!info.maxHp || info.maxHp == info.hp) showHp = info.hp;
                        else showHp = `${info.hp}/${info.maxHp}`;
                        let character = [info.sex, info.group, showHp, info.skills, info.tags];
                        lib.character[name] = character;
                        lib.translate[name] = info.translate;
                        lib.characterPack[`mode_extension_${type}_${extension}`][name] = character;
                        if (info.rank) {
                            if (typeof info.rank === "object") {
                                lib.rank.rarity[info.rank["rarity"]].push(name);
                            }
                            else lib.rank[info.rank].push(name);
                        }
                        if (info.des) {
                            lib.characterIntro[name] = info.des;
                        }
                        if (info.catelogy) {
                            if (!lib.characterSort[`mode_extension_${type}_${extension}`]) lib.characterSort[`mode_extension_${type}_${extension}`] = {};
                            if (!lib.characterSort[`mode_extension_${type}_${extension}`][`${type}_${extension}_${info.catelogy}`]) lib.characterSort[`mode_extension_${type}_${extension}`][`${type}_${extension}_${info.catelogy}`] = [];
                            lib.characterSort[`mode_extension_${type}_${extension}`][`${type}_${extension}_${info.catelogy}`].push(name);
                        }
                        if (info.replace) {
                            if (!lib.characterReplace[info.replace]) lib.characterReplace[info.replace] = [info.replace];
                            if (!lib.characterReplace[info.replace].contains(name)) lib.characterReplace[info.replace].push(name);
                        }
                    }

                    let catelogy = {
                        Standard: "神话再现",
                        Spirit: "一将之魂"
                    }

                    lib.translate[`mode_extension_${type}_${extension}_character_config`] = translate;
                    lib.translate[`${type}_${extension}`] = "雨落同清";
                    for (let name in catelogy) lib.translate[`${type}_${extension}_${name}`] = catelogy[name];
                }
            });
        },
        precontent: (_extension) => {
        },
        package: {
            intro: extInfo.intro,
            author: "Rintim",
            diskURL: "",
            forumURL: "",
            version: extInfo.version,
        },
    }
    return extension;
});